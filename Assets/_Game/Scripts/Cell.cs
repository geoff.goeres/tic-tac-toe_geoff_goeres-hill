﻿// <summary>
// This class manages cell setup, cell states, and cell events.
// </summary>

using System;
using UnityEngine;
using UnityEngine.UI;

public enum CellState
{
    Empty,
    X,
    O
}

public class Cell : MonoBehaviour
{
    [SerializeField] private Image playerMarker;

    private event Action<Cell> OnCellClick;

    public int Row { get; private set; }
    public int Col { get; private set; }
    public CellState Content { get; private set; }

    public bool IsEmpty => Content == CellState.Empty;

    public void Init(int row, int col, Action<Cell> onCellClickAction)
    {
        Row = row;
        Col = col;

        name = $"Cell [{row}, {col}]";

        OnCellClick = onCellClickAction;

        Clear();
    }

    public void Set(CellState cellState)
    {
        Content = cellState;

        switch (cellState)
        {
            case CellState.Empty:
                playerMarker.sprite = null;
                playerMarker.gameObject.SetActive(false);
                break;
            case CellState.X:
                playerMarker.sprite = IconSelection.Instance.GetSprite(0);
                playerMarker.gameObject.SetActive(true);
                break;
            case CellState.O:
                playerMarker.sprite = IconSelection.Instance.GetSprite(1);
                playerMarker.gameObject.SetActive(true);
                break;
            default:
                playerMarker.sprite = null;
                playerMarker.gameObject.SetActive(false);
                break;
        }
    }

    public void Clear()
    {
        Set(CellState.Empty);
    }

    public bool HasState(CellState cellState)
    {
        return Content == cellState;
    }

    public void OnClick()
    {
        OnCellClick?.Invoke(this);
    }
}