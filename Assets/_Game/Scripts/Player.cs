﻿// <summary>
// This class holds the player data.
// </summary>

public class Player
{
    private string Name { get; }
    private CellState State { get; }
    
    public int Score { get; set; }

    public Player(string name, int score, CellState state)
    {
        Name = name;
        Score = score;
        State = state;
    }

    public override string ToString()
    {
        return "Wins: " + Score;
    }
}