﻿// <summary>
// This class manages the main menu UI.
// </summary>

using UnityEngine;
using UnityEngine.UI;

public class MainMenu : BasePanel
{
    [SerializeField] private Dropdown boardSize;
    [SerializeField] private GameBoard gameBoard;

    protected override void Start()
    {
        base.Start();

        boardSize = GetComponentInChildren<Dropdown>();
        Show();
    }

    public void OnStartGame()
    {
        GameManager.Instance.Play();
    }

    public void OnSetBoardSize()
    {
        switch (boardSize.value)
        {
            case 0:
                gameBoard.SetBoardSize(GameBoard.BoardSize.Three);
                break;
            case 1:
                gameBoard.SetBoardSize(GameBoard.BoardSize.Four);
                break;
        }
    }

    public void OnQuitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif
        Application.Quit();
    }

    protected override void OnNewGame(GameManager game)
    {
        Hide();
    }

    protected override void OnGameQuit(GameManager game)
    {
        Show();
    }

    private void OnValidate()
    {
        if (gameBoard == null)
            gameBoard = FindObjectOfType<GameBoard>();
    }
}