﻿// <summary>
// This class is a base UI class for all other UI panels to derive from.
// </summary>

using UnityEngine;

public class BasePanel : MonoBehaviour
{
    private GameObject GameObject { get; set; }

    protected virtual void Awake()
    {
        GameObject = gameObject;
    }

    protected virtual void Show()
    {
        GameObject.SetActive(true);
    }

    protected virtual void Hide()
    {
        GameObject.SetActive(false);
    }

    protected virtual void Start()
    {
        Hide();

        GameManager.OnNewGame += OnNewGame;
        GameManager.OnGameResults += OnGameResults;
        GameManager.OnGameQuit += OnGameQuit;
    }

    protected virtual void OnNewGame(GameManager game)
    {
    }

    protected virtual void OnGameResults(GameManager game)
    {
    }

    protected virtual void OnGameQuit(GameManager game)
    {
    }
}