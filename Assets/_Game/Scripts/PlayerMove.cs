﻿// <summary>
// This struct holds the player move history data.
// </summary>

public struct PlayerMove
{
    private readonly CellState cellState;
    private readonly int row;
    private readonly int col;

    public PlayerMove(CellState cellState, int row, int col)
    {
        this.cellState = cellState;
        this.row = row;
        this.col = col;
    }

    public override string ToString()
    {
        return $"{cellState} placed a marker in cell: {row} , {col}";
    }
}