﻿// <summary>
// This class manages the game music.
// </summary>

using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class MusicPlayer : MonoBehaviour
{
    [SerializeField] [Tooltip("If selected the music player will play on awake.")]
    private bool playOnAwake;

    [SerializeField] [Tooltip("If selected the music player will loop the currently selected track.")]
    private bool loop;

    [SerializeField] [Tooltip("Drag music tracks from the project window into this list to create a music playlist.")]
    private AudioClip[] musicPlaylist = null;

    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = musicPlaylist[0];
        audioSource.playOnAwake = playOnAwake;
        audioSource.loop = loop;

        if (playOnAwake)
            audioSource.Play();
    }
}